#!/bin/sh -eu
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright 2019 (C) Olliver Schinagl <oliver@schinagl.nl>
#

set -eu

ENV_FILE="/tmp/env.bin"
ENV_PART="/dev/mtd0"
UPDATE_ENV_TOOL="/rigol/tools/cfger"


if [ -n "${1:-}" ]; then
    FLASH="${1}"
else
    if [ "$("${UPDATE_ENV_TOOL}" -c "bootpart" "A")" -eq 0 ]; then
        FLASH="B"
    elif [ "$("${UPDATE_ENV_TOOL}" -c "bootpart" "B")" -eq 0 ]; then
        FLASH="A"
    else
        echo "ERROR: Unable to parse bootpart from environment."
        exit 1
    fi
fi

case "${FLASH}" in
"A")
    BACKPART="B"
    BOOTPART="A"
    PART_BOOT_LOGO_OFF="0x4500000"
    PART_FIT_IMG_OFF="0x5100000"
    PART_ZYNQ_BIT_OFF="0x4900000"
    ;;
"B")
    BACKPART="A"
    BOOTPART="B"
    PART_BOOT_LOGO_OFF="0xe100000"
    PART_FIT_IMG_OFF="0x5100000"
    PART_ZYNQ_BIT_OFF="0xd900000"
    ;;
*)
    echo "Unknown flash image '${FLASH}'."
    exit 1
    ;;
esac
LOADADDR="0x3000000"
PART_FIT_IMG_COUNT="0x2000000"

"${UPDATE_ENV_TOOL}" -s "backpart" "${BACKPART}"
"${UPDATE_ENV_TOOL}" -s "bootcmd" "if run nandboot${BOOTPART}; then echo 'ok'; else setenv bootpart ${BACKPART};save;run nandboot${BACKPART}; fi"
"${UPDATE_ENV_TOOL}" -s "bootlogo" "loadlogo ${PART_BOOT_LOGO}"
"${UPDATE_ENV_TOOL}" -s "bootpart" "${BOOTPART}"
"${UPDATE_ENV_TOOL}" -s "nandbootA" "checkGTP;loadzynq ${PART_ZYNQ_BIT};ledoff;loadlogo ${PART_BOOT_LOGO};nand read ${LOADADDR} ${PART_FIT_IMG} ${PART_FIT_IMG_COUNT};bootm ${LOADADDR}"

flash_eraseall "${ENV_PART}"
nandwrite -p "${ENV_PART}" "${ENV_FILE}"

echo "Switched to flash image '${FLASH}'."

exit 0
